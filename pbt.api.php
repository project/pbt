<?php
/**
 * @file pbt.api.php
 */

/**
 * Alter the printable backtrace.
 */
function hook_pbt_alter(&$message, $backtrace) {
  $message .= t('Powered by love.');
}
