<?php
/**
 * @file pbt.settings.inc
 */

function pbt_settings_form($form, &$form_state) {
  $form['pbt_level'] = array(
    '#type' => 'radios',
    '#title' => t('Error messages to display'),
    '#default_value' => variable_get('pbt_level', pbt_level_default()),
    '#options' => pbt_level_options(),
    '#description' => t('Set backtrace level. Beware: Too many backtraces can produce nasty memory issues. You can override this level by using a query parameter like <em>www.page.org/to/debug?pbt=[0-3]</em>'),
  );
  return system_settings_form($form);
}
